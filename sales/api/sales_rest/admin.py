from django.contrib import admin

from .models import SaleRecord, SalesPerson

# Register your models here.
@admin.register(SalesPerson)
class AdminSalePerson(admin.ModelAdmin):
    pass

@admin.register(SaleRecord)
class AdminSaleRecord(admin.ModelAdmin):
    pass
