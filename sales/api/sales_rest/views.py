from django.http import JsonResponse
from django.shortcuts import render

# from inventory.api.inventory_rest.models import Automobile
from .models import SalesPerson, PotentialCustomer, AutoMobileVO, SaleRecord
import json
from django.views.decorators.http import require_http_methods

from common.json import ModelEncoder

class AutoMobileVODetailEncoder(ModelEncoder):
    model = AutoMobileVO
    properties = ["vin", "import_href", "sold"]


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["name", "address", "phone_number"]
    
    def get_extra_data(self, o):
        return ({"customer id": o.id})

class SalePersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number"]
    
    def get_extra_data(self, o):
        return ({"sales_person_id": o.id})

class SaleRecordListEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["automobile", "potentialCustomer", "price", "salesperson"]
    encoders = {"automobile": AutoMobileVODetailEncoder(), "potentialCustomer": PotentialCustomerEncoder(), "salesperson": SalePersonEncoder()}

    def get_extra_data(self, o):
        return ({"sales person": o.salesperson.name, "saleid": o.id})



@require_http_methods(["GET", "POST"])
def SaleRecordList(request, saleperson_id=None):
    if request.method == "GET":
        if saleperson_id is not None:
            salerecords = salerecord.objects.filter(salesperson = saleperson_id)
        else:
            salerecords = SaleRecord.objects.all()
        return JsonResponse({"salerecords": salerecords}, encoder=SaleRecordListEncoder)
    else:
        content = json.loads(request.body)
        try:
            potentialCustomer = PotentialCustomer.objects.get(phone_number=content["potentialCustomer"])
            content["potentialCustomer"] = potentialCustomer
            salesperson = SalesPerson.objects.get(employee_number=content["salesperson"])
            content["salesperson"] = salesperson
            vin = content["automobile"]
            automobile = AutoMobileVO.objects.get(vin=vin)
            # if automobile.sold == True:
            #     return JsonResponse({"message": "Vehicle already sold"}, status=400)
            # print ("beforehand", automobile.sold)
            # automobile.sold = True
            # automobile.save()
            print(automobile.id, automobile.sold)
            content["automobile"] = automobile

        except AutoMobileVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Automobile id"}, status=400)
        salerecord = SaleRecord.objects.create(**content)
        print("created object")
        return JsonResponse(salerecord, encoder=SaleRecordListEncoder, safe=False)

# def SaleRecordList(request):2
#     if request.method == "GET":
#             salerecords = SaleRecord.objects.all()
#             return JsonResponse({"sale records": salerecords}, encoder=SaleRecordListEncoder)
#     else:
#         content = json.loads(request.body)
#         try:
#             automobile_href = content["automobile"]
#             automobile = AutoMobileVO.objects.get(import_href=automobile_href)
#             content["automobile"] = automobile
#         except AutoMobileVO.DoesNotExist:
#             return JsonResponse({"message": "Invalid Automobile id"}, status=400)
#         salerecord = SaleRecord.objects.create(**content)
#         return JsonResponse(salerecord, encoder=SaleRecordListEncoder, safe=False)

@require_http_methods(["GET", "POST"])
def SalePersonList(request):
    if request.method == "GET":
        salepersons = SalesPerson.objects.all()
        return JsonResponse({"salespeople": salepersons}, encoder=SalePersonEncoder, safe=False)
    else:
        content = json.loads(request.body)
        salepersons = SalesPerson.objects.create(**content)
        return JsonResponse({"salespeople": salepersons}, encoder=SalePersonEncoder, safe=False)
    
def PotentialCustomerList(request):
    if request.method == "GET":
        potentialcustomers= PotentialCustomer.objects.all()
        return JsonResponse({"potential_customers": potentialcustomers}, encoder=PotentialCustomerEncoder, safe=False)
    else: 
        content = json.loads(request.body)
        potentialcustomer = PotentialCustomer.objects.create(**content)
        return JsonResponse({"potential_customer": potentialcustomer}, encoder=PotentialCustomerEncoder, safe=False)
# Create your views here.
