from django.contrib import admin
from django.urls import path, include
from .views import PotentialCustomerList, SalePersonList, SaleRecordList

urlpatterns = [path('salerecords/', SaleRecordList, name='api_create_salerecord'),
path("salespersons/<int:saleperson_id>/salerecord/", SaleRecordList, name="api_list_salerecords"), 
path("salespersons/", SalePersonList, name="api_create_saleperson"),
path("potentialcustomers/", PotentialCustomerList, name="api_create_potentialcustomer")]