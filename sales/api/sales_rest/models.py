from django.db import models
from django.forms import IntegerField

# from inventory.api.inventory_rest.models import Automobile

class SalesPerson(models.Model):
    name = models.CharField(max_length= 100)
    employee_number = models.CharField(max_length=50, null=True, unique=True)
    # salerecord = models.ForeignKey("SaleRecord", related_name="SalesPersons", on_delete=models.PROTECT )
    def __str__(self):
        return str(self.name)

class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15, unique=True)

class AutoMobileVO(models.Model):
    vin = models.CharField(max_length = 100)
    import_href = models.CharField(max_length = 100)
    sold = models.BooleanField(default=False)

class SaleRecord(models.Model):
    automobile = models.ForeignKey(AutoMobileVO, related_name="SaleRecords", on_delete=
    models.PROTECT)
    potentialCustomer= models.ForeignKey(PotentialCustomer, related_name="SalesRecords", on_delete=models.PROTECT)
    price = models.IntegerField()
    salesperson = models.ForeignKey(SalesPerson, related_name="SaleRecords", on_delete=models.PROTECT, null=True)
# Create your models here.
