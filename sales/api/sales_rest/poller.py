import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from sales_rest.models import AutoMobileVO

def get_salerecords():
    response = requests.get("http://localhost:8100/api/automobiles/")
    content = json.loads(response.content)
    for automobile in content["automobiles"]:
        print("hella")
        AutoMobileVO.objects.update_or_create(import_href=automobile["href"], defaults={"model": automobile["model"]})


def poll():
    while True:
        print("something")
        try:
            get_salerecords()
        except Exception as e:
            print(e)
        time.sleep(5)


if __name__ == "__main__":
    poll()
