# Generated by Django 4.0.3 on 2022-06-20 06:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_remove_salerecord_salesperson'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='salesperson',
            name='salerecord',
        ),
        migrations.AddField(
            model_name='salerecord',
            name='salespersons',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='SaleRecords', to='sales_rest.salesperson'),
        ),
    ]
