import React, { useEffect, useState} from "react";


export default function ManufacturersList(){
    let [manufacturers, setManufacturers] = useState([])
    useEffect(()=>{
        fetch('http://localhost:8100/api/manufacturers/')
          .then(response => response.json())
          .then(response => setManufacturers(response.manufacturers))
          .catch(e => console.error(e));

    }, [])

    return (<table className = "table" key = "table">
    <thead>
        <tr>
            <th key = "manufacturer-col" scope="col">Manufacturer</th>

        </tr>
    </thead>
    <tbody>
        {manufacturers.map(manufacturer => {
            return (
        <tr key = {manufacturer.id}>
            <td key = {manufacturer.id} scope="row">{manufacturer.name}</td>

        </tr>
        )})}
    </tbody>

 </table>)

}