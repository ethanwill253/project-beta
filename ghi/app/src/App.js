import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CustomerForm from './CustomerForm';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import ServiceForm from './ServiceForm';
import ServiceList from "./ServiceList";
import ServiceHistory from "./ServiceHistory";
import AutoForm from "./AutoForm";
import AutoList from "./AutoList";
import SaleRecordForm from './SaleRecordForm';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonHistory from './SalesPersonHistory';
import SaleRecordList from './SalesRecordList';
import ManufacturersList from './manufacturers';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';



function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians" element={<TechnicianForm />} />
          <Route path="services" element={<ServiceList />} />
          <Route path="services">
            <Route path="new" element={<ServiceForm />} />
          </Route>
          <Route path="services">
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="automobiles" element={<AutoList />} />
          <Route path="automobiles">
            <Route path="new" element={<AutoForm />} />
          </Route>
          <Route path ="/salerecords" element = {<SaleRecordList  />}/>
          <Route path = "/salerecords/create" element={<SaleRecordForm  />}/>
          <Route path = "/customers/create" element={<CustomerForm/>}/>
          <Route path = "/salespeople/" element = {<SalesPersonHistory/>}/>
          <Route path = "/salespeople/create" element = {<SalesPersonForm/>}/>
          <Route path = "/manufacturers" element = {<ManufacturersList/>}/>
          <Route path = "/manufacturers/create" element = {<ManufacturerForm/>}/>
          <Route path = "vehiclemodels/create" element = {<VehicleModelForm/>}/>
          <Route path = "vehiclemodels/" element = {<VehicleModelList/>}/>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
