import React from 'react';


export default class TechnicianForm extends  React.Component {
  constructor(props) {
    super(props)
    this.state = {
     name: ''
    }
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({name: value})
  }

  async handleSubmit(event) {
    event.preventDefault()
    const data = {...this.state}

    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);

    if (response.ok) {
      const newSTechnician = await response.json();
      console.log("New Technician data: ",newSTechnician);
      // const cleared = {
      //   name: ''
      // };
      window.location.reload();
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new technician</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text"  value={this.state.name} name="name" id="name" className="form-control"/>
                <label htmlFor="name">
                Technician name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


