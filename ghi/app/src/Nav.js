import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/"> Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/salerecords"> Sale Records</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/salerecords/create"> Sale Record Create</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/customers/create"> Add Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/salespeople/"> Salespeople sale history</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/salespeople/create"> Add Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/manufacturers"> Manufacturer List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/manufacturers/create"> Manufacturer Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/vehiclemodels/create"> Vehicle Model Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/vehiclemodels/"> Vehicle Model List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/technicians/"> Create technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/services/">Service list</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/services/new/">Create service</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/services/history">Service history</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/automobiles/">Automobiles list</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id = "nav" aria-current="page" to="/automobiles/new/">Create automobile</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
