import React, { useEffect, useState} from "react";
import { useForm } from "react-hook-form";



export default function AutoForm(){
    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
    const [models, setModels] = useState([])
    useEffect(()=>{
        fetch('http://localhost:8100/api/models/')
          .then(response => response.json())
          .then(response => setModels(response.models))
          .catch(e => console.error(e));

    }, [])
    const onSubmit = async function(data){
        console.log("submit button hit")
        console.log(data)
        const vehicleURL = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
          method: 'post',
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          }
        }
        const response = await fetch(vehicleURL, fetchConfig);
        if (response.ok){
          console.log("response ok")
          reset()
        }
        else{
          console.log("nonononono")
        }
      }
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
          <h1>Create a new automobile</h1>
          {/* "handleSubmit" will validate your inputs before invoking "onSubmit" */}
            <form onSubmit={handleSubmit(onSubmit)}>
              {/* register your input into the hook by invoking the "register" function */}
              {/* include validation with required or other standard HTML validation rules */}
              <div>Vehicle VIN</div>
                <input {...register("vin", { required: true })} />
              <div>Year</div>
                <input {...register("year", {required: true})} />
              <div>Color</div>
                <input {...register("color", {required: true})} />
              <div>Model</div>
              <select {...register("model_id")} id = "model-select" className="form-select">
                <option >Choose a model</option>
                {models && models.map(model => {
                return (
                  <option key = {model.id} value ={model.id}>
                      {model.name}
                  </option>)})}
              </select>
              <input type="submit" />
            </form>
          </div>
        </div>
      </div>
      )






}
