import React from 'react';
import ReactDOM from 'react-dom/client';
import { Route, Routes } from 'react-router-dom';


export default class ServiceForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      customerName: '',
      description: '',
      date: '',
      techNames: [],
      vin: ''
    }

    this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleTechNameChange = this.handleTechNameChange.bind(this);
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCustomerNameChange(event) {
    const value = event.target.value;
    this.setState({customerName: value})
  }

  handleDescriptionChange(event) {
    const value = event.target.value;
    this.setState({description: value})
  }

  handleDateChange(event) {
    const value = event.target.value;
    this.setState({date: value})
  }

  handleTechNameChange(event) {
    const value = event.target.value;
    this.setState({tech_name: value})
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({vin: value})
  }

  async handleSubmit(event) {
    event.preventDefault()
    const data = {...this.state}
    data.customer_name = data.customerName
    delete data.customerName
    delete data.techNames

    const serviceUrl = 'http://localhost:8080/api/services/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      },
    };
    const response = await fetch(serviceUrl, fetchConfig);
    console.log(response);

    if (response.ok) {
      const newService = await response.json();
      console.log("New service data: ",newService);
      // const cleared = {
      //   customerName: '',
      //   description: '',
      //   date: '',
      //   techNames: [],
      //   vin: ''
      // };
      window.location.reload();
    }
  }

  async componentDidMount() {
    // const inventory_url = 'http://localhost:8100/api/automobiles/';
    const technician_url = 'http://localhost:8080/api/technicians/';

    // const response = await fetch(inventory_url);
    const tech_response = await fetch(technician_url);
    // console.log(response);

    if ( tech_response.ok) {
        // const data = await response.json();
        const tech_data = await tech_response.json()
        console.log("tech data",tech_data)
        // console.log(data);
        // this.setState({inventory: data.autos})
        this.setState({techNames: tech_data.technicians})
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new service</h1>
            <form onSubmit={this.handleSubmit} id="create-service-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleCustomerNameChange} placeholder="Customer name" required type="text"  value={this.state.customerName} name="customer_name" id="customer_name" className="form-control"/>
                <label htmlFor="customer_name">
                Customer name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleDescriptionChange} placeholder="Description" required  type="text" value={this.state.description} name="description" id="description" className="form-control" />
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleDateChange} placeholder="Date" required  type="datetime-local"  value={this.state.date} name="date" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleTechNameChange} required id="tech_name" value={this.state.tech_name} name="tech_name" className="form-select">
                    <option value="">Choose a technician</option>
                    {this.state.techNames.map(tech_name => {
                      return (
                        <option value={tech_name.employee_number} key={tech_name.employee_number}>
                          {tech_name.name}
                        </option>
                      );
                    })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleVinChange} placeholder="VIN" required type="text"  value={this.state.vin} name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">
                VIN</label>
              </div>
              {/* <div className="mb-3">
                <select onChange={this.handleVinChange} required id="vin" value={this.state.vin} name="vin" className="form-select">
                    <option value="">Choose a vin</option>
                    {this.state.inventory.map(vin => {
                      return (
                        <option value={vin.vin} key={vin.vin}>
                          {vin.vin}
                        </option>
                      );
                    })}
                </select>
              </div> */}
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
