import React, { useEffect, useState} from "react";
import { useForm } from "react-hook-form";



export default function CustomerForm(){
    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
    const onSubmit = async function(data){
      console.log("submit button hit")
      console.log(data)
      const potentialCustomerURL = 'http://localhost:8090/api/potentialcustomers/'
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
      }
      const response = await fetch(potentialCustomerURL, fetchConfig);
      if (response.ok){
        console.log("response ok")
        reset()
      }
      else{
        console.log("nonononono")
      }
    }


    return (
        /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
        <form onSubmit={handleSubmit(onSubmit)}>
          {/* register your input into the hook by invoking the "register" function */}         
          {/* include validation with required or other standard HTML validation rules */}
          <div>Customer name</div>
          <input {...register("name", { required: true })} />
          <div>Customer phone number</div>
          <input {...register("phone_number", {required: true})} />
          <div>Customer address</div>
          <input {...register("address", {required: true})} />
          <input type="submit" />
          </form>)
}