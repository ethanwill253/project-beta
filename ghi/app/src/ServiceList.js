import React, { useState, useEffect } from 'react';



function ServiceList() {
  const [services, setServices] = useState({services: []})


  async function loadServices() {
    const response = await fetch('http://localhost:8080/api/services/');
    if (response.ok) {
      const data = await response.json();
      setServices(data)

    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    loadServices()
  }, [])

  const handleComplete = async service => {
    service["status"] = "Service complete"
    const serviceId = service["id"]

    const serviceUrl = `http://localhost:8080/api/services/${serviceId}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(service),
      headers: {
          'Content-Type': 'application/json',
      },
    };
    const response = await fetch(serviceUrl, fetchConfig);
    const newService = await response.json();
    console.log("Updated service data: ",newService);
    window.location.reload();

  }

  const handleCancel = async service => {
    service["status"] = "Service canceled"
    const serviceId = service["id"]

    const serviceUrl = `http://localhost:8080/api/services/${serviceId}/`;
    console.log(serviceUrl);
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(service),
      headers: {
          'Content-Type': 'application/json',
      },
    };
    const response = await fetch(serviceUrl, fetchConfig);
    console.log(response);
    const newService = await response.json();
    console.log("Updated service data: ",newService);
    window.location.reload();

  }


  return (
    <table className="table table-striped" key = "table">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
        {services.services.map(service => {
          return ( service.vip === true ? (
            <tr  key={service.href}>
              <td>⭐️{ service.vin }</td>
              <td>{ service.customer_name }</td>
              <td>{ service.date.slice(0,10) }</td>
              <td>{ service.time }</td>
              <td>{ service.tech_name }</td>
              <td>{ service.description }</td>
              <td>
                <button className="btn btn-primary active" onClick={() => handleComplete(service)}>Service complete</button>
              </td>
              <td>
                <button className="btn btn-danger active" onClick={() => handleCancel(service)}>Cancel service</button>
              </td>
            </tr>) : ( <tr  key={service.href}>
              <td>{ service.vin }</td>
              <td>{ service.customer_name }</td>
              <td>{ service.date.slice(0,10) }</td>
              <td>{ service.time }</td>
              <td>{ service.tech_name }</td>
              <td>{ service.description }</td>
              <td>
                <button className="btn btn-primary active" onClick={() => handleComplete(service)}>Service complete</button>
              </td>
              <td>
                <button className="btn btn-danger active" onClick={() => handleCancel(service)}>Cancel service</button>
              </td>
            </tr>)
          );
        })}
        </tbody>
    </table>
  )
}


export default ServiceList;