import React, { useEffect, useState} from "react";


export default function AutoList() {
  const [autos, setAutos] = useState([])

  useEffect(() => {
    fetch('http://localhost:8100/api/automobiles/')
      .then(response => response.json())
      .then(response => setAutos(response.autos))
      .catch(error => console.log(error))

  }, [])

  return(
    <>
    <h1>Automobile list</h1>
    <table  className="table table-striped" key = "table">
      <thead>
          <tr>
              <th key = "vin-col" scope="col">VIN</th>
              <th key = "year-col" scope="col">Year</th>
              <th key = "color-col" scope="col">Color</th>
              <th key = "model-col" scope = "col">Model </th>
          </tr>
      </thead>
      <tbody>
          {autos.map(auto => {
              return (
          <tr key = {auto.id}>
              <td key = {auto.vin}>{auto.vin}</td>
              <td key = {auto.year}>{auto.year}</td>
              <td  key = {auto.color}>{auto.color}</td>
              <td key = {auto.model.name}> {auto.model.name} </td>
          </tr>
          )})}
      </tbody>
    </table>
    </>
 )

}