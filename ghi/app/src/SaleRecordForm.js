import React, { useEffect, useState} from "react";
import { useForm } from "react-hook-form";


export default function SaleRecordForm(){
    let [salesPeople, setSalesPeople] = useState([])
    let [automobiles, setAutomobiles] = useState([])
    let [customers, setCustomers] = useState([])
    let [autoDetail, setAutoDetail] = useState()



    // const response = await fetch('http://localhost:8090/api/salespersons/');
    // useEffect(() =>{setSalesPeople(loadSalesPeople(), [])})
    useEffect(()=>{
      fetch('http://localhost:8090/api/salespersons/')
        .then(response => response.json())
        .then(response => setSalesPeople(response.salespeople))
        .catch(e => console.error(e));
      fetch('http://localhost:8100/api/automobiles/')
      .then(response => response.json())
      .then(response => setAutomobiles(response.autos))
      .catch(e => console.error(e));
      fetch('http://localhost:8090/api/potentialcustomers/')
        .then(response => response.json())
        .then(response => setCustomers(response.potential_customers))
        .catch(e => console.error(e));
    },[]) //set it for nothing so it only runs on load (component mount) rather than when any state changes
    // useEffect(()=>console.log(salesPeople), [salesPeople])
    // const data = await response.json()
    // console.log(data)

    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
    const onSubmit = async function(data){
      let automobilevin = data.automobile
          console.log("put function started")
          console.log(data)
          const automobileURL = `http://localhost:8100/api/automobiles/${automobilevin}/`
          fetch(automobileURL)
          .then(response => response.json())
          .then(response => setAutoDetail(response))
      console.log("submit button hit")
      console.log(data)
      const saleRecordURL = 'http://localhost:8090/api/salerecords/'
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
      }
      const response = await fetch(saleRecordURL, fetchConfig);
      console.log(response.body)
      if (response.ok){
        console.log("response ok")
          console.log(automobileURL)
          console.log(autoDetail)
          const fetchConfig = {
            method: 'PUT',
            body: JSON.stringify({"sold": true}),
            headers: {
                'Content-Type': 'application/json',
            }
          }
          const response = await fetch(automobileURL, fetchConfig);
     
          if (response.ok){
            console.log(response)
            console.log("successfully put sold to true")
            window.location.reload();
          }
          else{
            console.log("failed put")
        }
      }
        reset()
      }
      
  
//   const onSubmitPUT = async function(data){
//     data["sold"] = true
//     let automobilevin = data.automobile
//     console.log("put function started")
//     console.log(data)
//     const automobilesURL = `http://localhost:8100/api/automobiles/${automobilevin}/`
//     const fetchConfig = {
//       method: 'put',
//       body: JSON.stringify(data),
//       headers: {
//           'Content-Type': 'application/json',
//       }
//     }
//     const response = await fetch(automobilesURL, fetchConfig);
//     if (response.ok){
//       console.log("successfully put sold to true")
      
//     }
//     else{
//       return (<div>already sold</div>)
//   }
// }

    return (
        /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
        <form onSubmit={handleSubmit(onSubmit) } >
          {/* register your input into the hook by invoking the "register" function */}
          {/* include validation with required or other standard HTML validation rules */}
          <div>Price</div>
          <input {...register("price", { required: true })} />
          {/* errors will return when field validation fails  */}
          {errors.exampleRequired && <span>This field is required</span>}
          <select {...register("potentialCustomer")} id = "customer-select" className="form-select">
            <option >Choose a customer</option>
            {customers && customers.map(customer => {
            return (
              <option key = {customer.id} value ={customer.phone_number}>
                  {customer.name} {customer.phone_number}
              </option>)})}
         </select>
          <select {...register("automobile")} id = "automobile-select" className="form-select">
            <option >Choose an automobile</option>
            {automobiles && automobiles.filter(automobile => automobile.sold == false).map(automobile => {
            return (
              <option key = {automobile.id} value ={automobile.vin}>
                  {automobile.color} {automobile.year} {automobile.model.name} {automobile.vin}
              </option>)})}
         </select>
          <select {...register("salesperson")} id = "salesperson-select" className="form-select">
            <option >Choose a sales person</option>
            {salesPeople && salesPeople.map(salesperson => {
            return (
              <option key = {salesperson.sales_person_id} value ={salesperson.employee_number}>
                  {salesperson.name} {salesperson.employee_number}
              </option>)})}
         </select>
          <input type="submit" />
        </form>
      );
    

  }