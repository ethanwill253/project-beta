


import React, { useEffect, useState} from "react";
import { useForm } from "react-hook-form";



export default function VehicleModelForm(){
    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
    let [manufacturers, setManufacturers] = useState([])
    useEffect(()=>{
        fetch('http://localhost:8100/api/manufacturers/')
          .then(response => response.json())
          .then(response => setManufacturers(response.manufacturers))
          .catch(e => console.error(e));
          
    }, [])
    const onSubmit = async function(data){
        console.log("submit button hit")
        console.log(data)
        const vehiclemodelsURL = 'http://localhost:8100/api/models/'
        const fetchConfig = {
          method: 'post',
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          }
        }
        const response = await fetch(vehiclemodelsURL, fetchConfig);
        if (response.ok){
          console.log("response ok")
          reset()
        }
        else{
          console.log("nonononono")
        }
      }
    return (
        /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
        <form onSubmit={handleSubmit(onSubmit)}>
          {/* register your input into the hook by invoking the "register" function */}         
          {/* include validation with required or other standard HTML validation rules */}
          <div>Vehicle model name</div>
          <input {...register("name", { required: true })} />
          <div>Picture URL</div>
          <input {...register("picture_url", {required: true})} />
          <select {...register("manufacturer_id")} id = "manufacturer-select" className="form-select">
            <option >Choose a manufacturer</option>
            {manufacturers && manufacturers.map(manufacturer => {
            return (
              <option key = {manufacturer.id} value ={manufacturer.id}> 
                  {manufacturer.name} 
              </option>)})} 
         </select>
          <input type="submit" />
          </form>)






}