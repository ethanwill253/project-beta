import React, { useEffect, useState} from "react";
import { useForm } from "react-hook-form";



export default function ManufacturerForm(){
    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
    const onSubmit = async function(data){
      console.log("submit button hit")
      console.log(data)
      const manufacturersURL = 'http://localhost:8100/api/manufacturers/'
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
      }
      const response = await fetch(manufacturersURL, fetchConfig);
      if (response.ok){
        console.log("respect+")
        reset()
      }
      else{
        console.log("Failure")
      }
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
          <div>Manufacturer name</div>
          <input {...register("name", { required: true })} />

          <input type="submit" />
          </form>)


}
