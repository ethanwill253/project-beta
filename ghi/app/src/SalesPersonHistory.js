import React, { useEffect, useState} from "react";
import { useForm } from "react-hook-form";



export default function SalesPersonHistory(){

    let [salesPeople, setSalesPeople] = useState([])
    let [saleRecords, setSaleRecords] = useState([])
    let [FocusedSP, setFocusedSP] = useState(0)
    
    useEffect(() =>{
        fetch('http://localhost:8090/api/salespersons/')
        .then(response => response.json())
        .then(response => setSalesPeople(response.salespeople))
        .catch(e => console.error(e));
        fetch('http://localhost:8090/api/salerecords/')
        .then(response => response.json())
        .then(response => setSaleRecords(response.salerecords))
        .catch(e => console.error(e));
    }, []
    )
    // const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
    return(
    <> 
    <select onChange = {x => setFocusedSP(x.target.value)} id = "salesperson-select" className="form-select">
    <option >Choose a sales person</option>
    {salesPeople && salesPeople.map(salesperson => {
    return (
      <option key = {salesperson.sales_person_id} value ={salesperson.employee_number} > 
          {salesperson.name} {salesperson.employee_number}
      </option>)})} 
 </select>
 <table className = "table">
    <thead>
        <tr>
            <th key = "sales header" scope="col">Sales Person</th>
            <th key = "customer header" scope="col">Customer</th>
            <th key = "vin header" scope="col">VIN</th>
            <th key = "sale price header" scope="col">Sale Price</th>
        </tr>
    </thead>
 {saleRecords.filter(saleRecord => saleRecord.salesperson.employee_number == FocusedSP).map(filteredSR =>{
 return (
 
    <tbody>
        <tr key = {filteredSR.id}>
            <td key = {filteredSR.salesperson.sales_person_id} scope="row">{filteredSR.salesperson.name}</td>
            <td key = {filteredSR.potentialCustomer.phone_number}>{filteredSR.potentialCustomer.name}</td>
            <td key = {filteredSR.automobile.vin}>{filteredSR.automobile.vin}</td>
            <td key = {filteredSR.salesperson.employee_number}>{filteredSR.price}</td>
        </tr>
    </tbody>
 
 )

 })}
  </table>
 </>
 )
}