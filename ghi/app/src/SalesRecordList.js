import React from 'react';
import { useEffect, useState} from "react";
// import ReactDOM from 'react-dom/client';
// import App from './App';

function SaleRecordList(props){
  let [saleRecords, setSaleRecords] = useState([])
  useEffect(() =>{
    fetch('http://localhost:8090/api/salerecords/')
    .then(response => response.json())
    .then(response => setSaleRecords(response.salerecords))
    .catch(e => console.error(e));
}, []
)


    return(
      <React.Fragment key="fragment">
      <h1 key = "header">Sales records</h1>
        <table className="table table-striped" key = "table">
          <thead key = "table-head">
            <tr key = "table-row">
              <th key = "vin">VIN</th>
              <th key = "customer">Customer</th>
              <th key = "sales-person">Sales person</th>
            </tr>
          </thead>
          <tbody key = "table-body">
            {saleRecords.map(salerecord => {
            return (

              <tr key={salerecord.saleid}>
                <td key = {salerecord.automobile.vin}>{salerecord.automobile.vin}</td>
                <td key = {salerecord.potentialCustomer.phone_number}>{salerecord.potentialCustomer.name}</td>

                <td key = {salerecord.salesperson.name}>{salerecord.salesperson.name}</td>
              </tr>

            );
            })}
          </tbody>
        </table>
        </React.Fragment>
        )
  }

  export default SaleRecordList;