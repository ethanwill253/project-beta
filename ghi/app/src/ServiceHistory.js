import React, { useState, useEffect } from 'react';
// import { Card, Button, Input } from 'react-bootstrap';


export default function ServiceHistory() {
  const [searchVin, setSearchVin] = useState("");
  const [filteredResults, setFilteredResults] = useState([]);

  const [services, setServices] = useState({services: []})

  async function loadServices() {
    const response = await fetch('http://localhost:8080/api/services/history/');
    if (response.ok) {
      const data = await response.json();
      console.log("index.js data: ",data)
      setServices(data)

    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    loadServices()
  }, [])


  const handleChange = event => {
    setSearchVin(event.target.value);
    const searchInput = event.target.value
    const filteredData = services.services.filter((item) => {
      return Object.values(item).join(' ').toLowerCase().includes(searchInput.toLowerCase())
    })
    console.log(filteredData)
    setFilteredResults(filteredData)
  };

  return (
    <>
    <h1>Service Appointment History</h1>
    <div className="row">
      <input type="text" placeholder="Search" value={searchVin} onChange={handleChange}/>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>VIP</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
            {/* <th>Delete</th> */}
          </tr>
        </thead>
        <tbody>
      {filteredResults.map(service => {
         return (
            <tr  key={service.href}>
              <td>{ service.vin }</td>
              <td>{ service.customer_name }</td>
              {/* <td>{ service.vip }</td> */}
              <td>{service.vip === false ? 'No'
                        : service.vip === true
                        ? 'Yes'
                        : service.vip}</td>
              <td>{ service.date.slice(0,10) }</td>
              <td>{ service.time }</td>
              <td>{ service.tech_name }</td>
              <td>{ service.description }</td>
              <td>{ service.status }</td>
            </tr>
          );
        })}
        </tbody>
      </table>
    </div>
    </>
  );
}