import React, { useEffect, useState} from "react";
import { useForm } from "react-hook-form";



export default function SalesPersonForm(){
    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
    const onSubmit = async function(data){
      console.log("submit button hit")
      console.log(data)
      const salespeopleURL = 'http://localhost:8090/api/salespersons/'
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
      }
      const response = await fetch(salespeopleURL, fetchConfig);
      if (response.ok){
        console.log("response ok")
        reset()
      }
      else{
        console.log("nonononono")
      }
    }


    return (
        /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
        <form onSubmit={handleSubmit(onSubmit)}>
          {/* register your input into the hook by invoking the "register" function */}         
          {/* include validation with required or other standard HTML validation rules */}
          <div>Salesperson name</div>
          <input {...register("name", { required: true })} />
          <div>Salesperson employee number</div>
          <input {...register("employee_number", {required: true})} />
          <input type="submit" />
          </form>)
          
}