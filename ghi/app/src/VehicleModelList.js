import React, { useEffect, useState} from "react";

export default function VehicleModelList(){
    let [models, setModels] = useState([])
    useEffect(()=>{
        fetch('http://localhost:8100/api/models/')
          .then(response => response.json())
          .then(response => setModels(response.models))
          .catch(e => console.error(e));
          
    }, [])
    return (<table className = "table" key = "table">
    <thead>
        <tr>
            <th key = "name-col" scope="col">Name</th>
            <th key = "manufacturer-col" scope = "col">Manufacturer </th>
            <th key = "image-col" scope = "col">image </th>
        </tr>
    </thead>
    <tbody>
        {models.map(model => {
            return (
        <tr key = {model.id}>
            <td key = {model.name} scope="row">{model.name}</td>
            <td key = {model.manufacturer.name} scope ="row"> {model.manufacturer.name} </td>
            <td key = {model.picture_url} scope ="row"> <img src={model.picture_url} width="283" height="130"/> </td>
        </tr>
        )})}
    </tbody>

 </table>)
}