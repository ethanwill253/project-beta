# CarCar

Team:

*Ethan Williams - Services
* Roger Wang - Sales

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

models - Technician, InventoryVO, Service
Technician only has two properties so it will be set up first.
It will have two views, those views will have one model encoder.
InventoryVO is the value object representation of the automobile api.
Service will have a foreign key to the technician. It will have two views and one model encoder.
I will create a urls file to add paths to my views.
I will use the poller to connect data from the autos api.
Once all my views and models are working I will start creating react js components.
Components to set up - Create technician, Create service form, Service list, Service history.

-/technician
-/service
-/service/new
-/service/history

To set up a VIP relationship with vehicles in the carcar inventory, checking
if my inventoryvo matches the vehicle that the service request is for.
I want to set a status on service to tell the api is the car service is complete or not.

Data Creation:

-Method PUT:
  -Manufactures:
    {
      "name": "Chrysler"
    }
  -Models:
    {
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer_id": 2
    }
  -Automobiles:
    {
      "color": "blue",
      "year": 2013,
      "vin": "1C3CC5FB2AN1hu765",
      "model_id": 2
    }
  -Technicians:
    {
      "name": "Burger"
    }
  -Services:
    {
			"customer_name": "Cheyenne",
			"description": "Change oil",
			"date": "2022-06-04T08:00:00",
			"vip": false,
			"status": "Service in progress",
			"vin": "1C3bg6y82AN1hu775",
			"tech_name": "Burger"
		},

## Sales microservice

models - SalesPerson, PotentialCustomer, AutoMobilevo, SalesRecord

Established what models have foreign key relationships and develop models in which don't have them first and those who have them after. (SalesRecord have 3 foreign key relationships, so I wrote that last. The other models have no foreignkey models, so I did those first in no particular order.)

I then wrote the poller which transferred over attributes which are necessary for the SalesRecord functionality.
The VIN is the key attribute for user to identify AutoMobiles, and the href is the one for the database to identify them.

I wrote my api views by not only analyzing the requirements of each form and list.

For React I fetched necesary data from microservices within each component using the useEffect hook. I wrote forms
using a hook called useForm, which is an external package. For lists, I did them with maps and for the sales person history list, I used a map with a filter.

I also added a sold attribute to the automobileVO model which defaults to false but becomes true if the automobile associated with an inputted sales record form. An already sold automobile cannot be sold again. Some may say you cannot edit a value object, but I still believe automobileVO is a value object because automobileVO isn't used until a sale occurs, and until then the data is static. When "sold" is updated, it is only when the data is being used does it change. It doesn't change outside of that. Another note is that while we could have established "sold" in the inventory microservice, it would require 10 times the work to poll back and forth the information required to have "sold" be in the inventroy project, rather than just be in the VO object of sales. It is the best decision to keep things as they are and have a MVP mindset until there is a reason to change it.


Edit:
Regarding the last paragraph, I decided to make sold exist on the inventory project. The reasoning being that I needed to check and update it on that end, and not the VO end. This is because the automobile mapping on my sales history form communicated with the inventory project's automobiles and not the VO objects. I wanted to decrease the list of available automobiles after submit.

