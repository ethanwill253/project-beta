from django.db import models
from django.urls import reverse


# Create your models here.
class Technician(models.Model):
  name = models.CharField(max_length=200)
  employee_number = models.AutoField(primary_key=True)

  def __str__(self):
      return self.name

class InventoryVO(models.Model):
  import_href = models.CharField(max_length=200, unique=True)
  vin = models.CharField(max_length=17, unique=True)


class Service(models.Model):
  customer_name =  models.CharField(max_length=200)
  description = models.TextField()
  date = models.DateTimeField()
  time = models.CharField(max_length=5, default="00:00")
  vip = models.BooleanField(default=False)
  status = models.CharField(max_length=200, default="Service in progress")
  tech_name = models.ForeignKey(
    Technician,
    related_name="services",
    on_delete=models.CASCADE,
    null=True,
    default=None
  )
  vin=models.CharField(max_length=17, blank=False, default="0000000000000000")

  def get_api_url(self):
      return reverse("api_service", kwargs={"pk": self.pk})

  class Meta:
      ordering = ("date", "time", "vip")
