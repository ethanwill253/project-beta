from django.shortcuts import render
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import Service, InventoryVO, Technician

# Create your views here.

class InventoryVOEncoder(ModelEncoder):
  model = InventoryVO
  properties = [
    "import_href",
    "vin"
  ]


class TechnicianEncoder(ModelEncoder):
  model = Technician
  properties = [
    "name",
    "employee_number"
  ]


class ServiceEncoder(ModelEncoder):
  model = Service
  properties = [
    "id",
    "customer_name",
    "description",
    "date",
    "time",
    "vip",
    "status",
    "vin",
  ]
  # encoders = {
  #   "tech_name": TechnicianEncoder(),
  #   "vin": InventoryVOEncoder(),
  # }
  def get_extra_data(self, o):
      return {
        "tech_name": o.tech_name.name
      }


@require_http_methods(["GET", "POST"])
def api_list_techs(request):
  if request.method == "GET":
    # if bin_vo_id is not None:
    #     shoes = Shoe.objects.filter(bin=bin_vo_id)
    # else:
      technicians = Technician.objects.all()
      return JsonResponse(
          {"technicians": technicians},
          encoder=TechnicianEncoder,
      )
  else:
    try:
      content = json.loads(request.body)
      technician = Technician.objects.create(**content)
      return JsonResponse(
          technician,
          encoder=TechnicianEncoder,
          safe=False,
      )
    except:
          response = JsonResponse(
              {"message": "Could not create new technician"}
          )
          response.status_code = 400
          return response


@require_http_methods(["DELETE", "PUT"])
def api_show_tech(request, pk):
  if request.method == "GET":
        technician = Technician.objects.get(employee_number=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
  if request.method == "DELETE":
      count, _ = Technician.objects.filter(employee_number=pk).delete()
      return JsonResponse({"deleted": count > 0})
  else:
      content = json.loads(request.body)
      Technician.objects.filter(employee_number=pk).update(**content)
      technician = Technician.objects.get(employee_number=pk)
      return JsonResponse(
          technician,
          encoder=TechnicianEncoder,
          safe=False,
      )


@require_http_methods(["GET", "POST"])
def api_services(request):
    if request.method == "GET":
        services = Service.objects.filter(status="Service in progress")
        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder,
        )
    else:
        content = json.loads(request.body)
        # try:

        date = content["date"][:10]
        time = content["date"][11:16]
        content["time"] = time
        content["date"] = date
        emp_num = content["tech_name"]
        tech = Technician.objects.get(employee_number=emp_num)
        content["tech_name"] = tech
        try:
          vin_num = content["vin"]
          vin_n = InventoryVO.objects.get(vin=vin_num)
          if vin_n is not None:
            content["vip"] = True
        except:
          pass
        service = Service.objects.create(**content)
        print(service)
        return JsonResponse(
            service,
            encoder=ServiceEncoder,
            safe=False,
        )
        # except:
        #     response = JsonResponse(
        #         {"message": "Could not create the service appointment"}
        #     )
        #     response.status_code = 400
        #     return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_service(request, pk):
    if request.method == "GET":
        try:
            service = Service.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            service = Service.objects.get(id=pk)
            service.delete()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            print("trying")
            content = json.loads(request.body)
            print("content from views: ", content)
            service = Service.objects.get(id=pk)

            props = ["status"]
            for prop in props:
                print(prop)
                if prop in content:
                    setattr(service, prop, content[prop])
            service.save()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_services_history(request):
      services = Service.objects.all()
      return JsonResponse(
          {"services": services},
          encoder=ServiceEncoder,
      )