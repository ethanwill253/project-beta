from django.urls import path


from .views import (
  api_list_techs,
  api_show_tech,
  api_services,
  api_service,
  api_services_history
)


urlpatterns = [
    path("technicians/", api_list_techs, name="api_list_techs"),
    path("technicians/<int:pk>/", api_show_tech, name="api_show_tech"),
    path("services/", api_services, name="api_services"),
    path("services/<int:pk>/", api_service, name="api_service"),
    path("services/history/", api_services_history, name="api_services_history"),
]

