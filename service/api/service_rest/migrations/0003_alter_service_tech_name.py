# Generated by Django 4.0.3 on 2022-06-20 20:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_technician_service_tech_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='tech_name',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='services_tech', to='service_rest.technician'),
        ),
    ]
